package com.example.a005141249csusbedu.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class MainActivity extends AppCompatActivity {
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String usd;
    private Double exchange;
    private static final String url = "https://api.fixer.io/latest?base=USD";
    String json = "";
    String line = "";
    String rate = "";
    @Override
    protected void onCreate (Bundle savedInstanceSate){
        super.onCreate(savedInstanceSate);
        setContentView(R.layout.activity_main);
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                System.out.println("\nTESTING 1 ... Before AsynchExecuteion \n");
                BackgroundTask retrieve_URL = new BackgroundTask();
                retrieve_URL.execute();
                System.out.println("\nTESTING 2 ... After AsynchExecution\n");

                //usd = editText01.getText().toString();
                //if (usd.equals("")){
                   // textView01.setText("This field cannot be blank");
                //}else {
                   // double dInputs = Double.parseDouble(usd);
                   // double result = dInputs * 112.57;
                    //textView01.setText("$" + usd + " = " + "¥" +String.format("%.2f",result));
                   // editText01.setText("");
                //}
            }
        });
    }
    private class BackgroundTask extends AsyncTask<Void,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
        protected void onPostExecute(String result){
            super.onPostExecute(result);
        }
        @Override
        protected String doInBackground(Void... params){
            try {
                URL web_url = new URL(MainActivity.this.url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                System.out.println("\nTesting\n");
                httpURLConnection.connect();
                InputStream inputStream= httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                System.out.println("CONNECTION SUCCESSFUL");
                while(line != null){
                    line = bufferedReader.readLine();
                    json += line;
                }
                System.out.println("\nTHE JSON" + json);
                JSONObject obj = new JSONObject(json);
                JSONObject objRate = obj.getJSONObject("rates");
                rate = objRate.get("JPY").toString();
                exchange = Double.parseDouble(rate);
                System.out.println("\nWhat is rate: " + rate + "\n");
                System.out.println("\n Testing KSON String Exchange Rate INSIDE AsynchTask");
                usd = editText01.getText().toString();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (usd.equals("")){
                            System.out.println("It crashes my boy");
                            textView01.setText("Cannot leave this blank");
                        } else {
                            Double dInputs = Double.parseDouble(usd);
                            Double result = dInputs * exchange;
                            System.out.println(usd + "" + result);
                            textView01.setText("$" +usd+ " = " + "Y" + String.format("%.2f",result));
                            editText01.setText("");
                        }
                    }
                });
            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e){
                Log.e("MYAPP",  "unexpected JSON exception",e);
                System.exit(1);
            }
            return null;
        }
    }
}

