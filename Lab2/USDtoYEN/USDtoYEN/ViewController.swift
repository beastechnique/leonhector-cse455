//
//  ViewController.swift
//  USDtoYEN
//
//  Created by Hector Leon on 1/14/18.
//  Copyright © 2018 Hector Leon. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var label: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func ConvertButton(sender: AnyObject) {
        //Get the input from user
        let textFieldValue = Double(textfield1.text!)
        //if statement to make sure user cannot leave this field blank
        if textFieldValue != nil {
            let result = Double(textFieldValue! * 112.57)
            label.text = "$\(textFieldValue!)= ¥\(result)"
            //empty textfield
            textfield1.text = ""
        }else{
            label.text="This field cannot be blank!"
        }
                
    }

}

